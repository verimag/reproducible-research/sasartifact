- [Artifact of the article **SASA: a SimulAtor of Self-stabilizing Algorithms** published in TAP 2020](#org8ae01f0)
  - [Using The TAP Virtual Machine](#org06c1764)
  - [Using Docker](#orgc68a614)
    - [Instructions to generate the data contained in Fig.1 of Section 2](#orgb862449)
    - [Instructions to generate the data contained in Table 1 of Section 4](#org31b988d)



<a id="org8ae01f0"></a>

# Artifact of the article **SASA: a SimulAtor of Self-stabilizing Algorithms** published in TAP 2020

The objective of this artifact is to show how to replicate the experiments mentioned in [this article](https://hal-cnrs.archives-ouvertes.fr/hal-02521149).

<https://doi.org/10.5281/zenodo.3751283>

This artifact contains:

-   instruction to install the necessary tools;
-   instructions to replay the interactive session described in Section 2 of the paper;
-   instructions to generate the data contained in Table 1 of Section 4.

By following the instructions, you should be able to replay the experiments, but for more information on how to use the toolset, please visit:

-   <https://verimag.gricad-pages.univ-grenoble-alpes.fr/vtt/tags/sasa/>
-   <https://verimag.gricad-pages.univ-grenoble-alpes.fr/synchrone/sasa/>


<a id="org06c1764"></a>

## Using The TAP Virtual Machine

All the instructions can be found here <http://www-verimag.imag.fr/reproducible-research/TAP2020_ARTIFACT>

They should be executed under a VM that you should find here: <https://doi.org/10.5281/zenodo.3751283>

cf <https://tap.sosy-lab.org/2020/callforartifacts.php>


<a id="orgc68a614"></a>

## Using Docker

You can mimic one the CI job defined in [.gitlab-ci.yml](https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/reproducible-research/sasartifact/blob/master/.gitlab-ci.yml). For instance, if you want to do as in the `expe-dockver` job, you just need to run docker using the `jahierwan/verimag-sync-tools` image (which is available in the cloud) as follows:

```sh
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/reproducible-research/sasartifact.git 
cd expe
docker run --user `id -u` \
    -v "$PWD":/current_dir -w /current_dir \
    -e DISPLAY=$DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix  \
    -i -t jahierwan/verimag-sync-tools $@
```

If the last command has run without problem, you are inside a docker image where:

-   all the necessary tools are installed
-   the `expe` directory (coming from the clone) is accessible

You can thus proceed with the instructions below.


<a id="orgb862449"></a>

### Instructions to generate the data contained in Fig.1 of Section 2

The implementation of Algorithm 1 (top of Page 3) can be found in [expe/async-unison/p.ml](https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/reproducible-research/sasartifact/blob/master/expe/async-unison/p.ml)

In order to replay the interactive session described in Section 2, you need to type the following commands in a terminal:

```sh
cd async-unison
make rdbg4
```

You ought to be prompted to type `<Enter>` or `q` and then `<Enter>` Choose the first proposal and press the `<Enter>` key; this creates a default session using commands defined in [expe/async-unison/Makefile](https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/reproducible-research/sasartifact/blob/master/expe/async-unison/Makefile) by the `rdbg4` rule.

You ought to see:

1.  A pdf view of 4x4 grid with enabled nodes in green, and active ones in orange. This pdf corresponds to the configuration 1 of Fig1 in the article. The active nodes (among the enabled ones) are not necessarily the same as in the article as we did not set the seed of the pseudo-random generator.
2.  the `(rdbg)` prompt in your terminal

At the `(rdbg)` prompt, type `sd` and `<Enter>`. This will make the simulation move 1 step forward, and update automatically the pdf view that shows up something equivalent to Configuration 2 in Fig1.

At the `(rdbg)` prompt, type `<Enter>` to replay the last command (i.e., `sd`), and hence move to Configuration 3.

Type `<Enter>` 3 more times to see Configurations 4, 5, and 6 of Fig.1.

Type `q` to exit.


<a id="org31b988d"></a>

### Instructions to generate the data contained in Table 1 of Section 4

In order to run the experiments described in Section 4, type in a terminal, from the [expe](https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/reproducible-research/sasartifact/blob/master/expe) directory:

```sh
make expe
```

It launches the experiments on 2 small graphs (`grid.dot` and `ER.dot`). It should last a few minutes. Once the previous command returns, you can launch:

```sh
find . -name \*.log
```

to see the generated log files. Those files contain the memory usage and the execution time of each individual experiment. If you launch:

```sh
make save_result
```

a (sed) script will parse those .log files to generate a `summary.org` file and save generated files in a directory named `results`. The summary.org file ought to contain a summary of all the experiments you have just performed. Table 1 was obtained out of it.

The correspondence between directory names and the Column 1 names is

-   bfs-spanning-tree : BFS
-   dfs : DFS-a
-   dfs-list: DFS-l
-   coloring : COL
-   unison : SYN
-   async-unison : ASY

The corresponding algorithms are encoded in file p.ml of each directory.

If you want to generate the experiment results for the 2 bigger graphs (`biggrid.dot` and `bigER.dot`), you need to be more patient (a few hours) and to launch:

```sh
make bigexpe
make save_result
```

Some remarks, comparing the results announced in the paper and the ones obtained on your machine:

-   The number of seconds (time/step) may differ as the table was obtained using an other machine.
-   Some examples may run out of memory on your machine: the dfs algo on `hugeER.dot` and `biggrid.dot`. Indeed, as one can notice in Table 1, those examples require a lot of memory (6.6 and 29 G) that your machine may not have.
