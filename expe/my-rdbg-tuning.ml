(*  Some useful short-cuts for rdbg interactive sessions *)
#require "sasa";;

 (* you need to install the emacs highligth package available in melpa *)
let hl hook (file, line, charb, chare) =
  let cmd = Printf.sprintf "emacsclient -e '(save-selected-window (find-file-other-window \"%s\") %s (goto-char (point-min)) (forward-line %i)  (forward-char %i) (set-mark-command nil) (forward-char %i) (hlt-highlight-region) )' "
 file hook (line-1) charb (max 1 (chare-charb))
  in
  ignore(Sys.command cmd)

let hl_atoms = function
  | []-> ()
  | x::t -> hl "(hlt-unhighlight-region)" x; List.iter (hl "") t
(*| x::t -> hl "(hlt-unhighlight-all-prop t)" x; List.iter (hl "") t *)
        
let _ = 
  let cmd = "emacsclient -e ' (hlt-choose-default-face \"org-checkbox\") '" in
  ignore(Sys.command cmd)

let emacs = ref false 

let emacs_udate e =
  if !emacs then
  try 
  match e.sinfo with
  | None -> ()
  | Some si ->
     let si_list =
       List.map
         (fun si -> (si.file, fst si.line, fst si.char, snd si.char))
         (si()).atoms
     in
     hl_atoms si_list
  with
    Match_failure _  -> ()

(**********************************************************************)
(* to implement the undo command *)
let redos = ref [1];;
let store i = redos := i::!redos;;
                                                                  
(**********************************************************************)
(**********************************************************************)
(* print_event tuning *)

(* split  the vars into returns  (the enab vars, the  activated vars,
   the other vars) nb: in the "Enab" prefix is removed from enab vars
   names; ie we leave only the pid and the action name *)
type s = (string * string * Data.v) 
let split_data (l:Data.subst list) : s list * s list * s list = 
    let l = List.map (fun (x,v) -> Str.split (Str.regexp "_") x, v) l in
    let rec sortv (enab, other) (x,v) =
      match x with 
      | "Enab"::pid::tail -> (pid, String.concat "_" tail,v)::enab, other
      | pid::tail -> enab, (pid,(String.concat "_" tail),v)::other
      | [] -> assert false
    in
    let enab, other = List.fold_left sortv ([],[]) l in
    let acti_names = List.map (fun (pid, n, v) -> pid, n) enab in  
    let act, vars = List.partition (fun (pid,n, _) -> List.mem (pid,n) acti_names) other in
    enab, act, vars

let only_true l = List.filter (fun (_,_, v)  -> v = B true) l
(* Only print the active process values *)
let str_of_sasa_event e =
    let enab, act, vars = split_data e.data in
    (*     let enab = only_true enab in *)
    let act = only_true act in
    let act_pid = List.map (fun (pid,_,_) -> pid) act in
    let vars = List.filter (fun (pid, _,_) -> List.mem pid act_pid) vars in
    let _to_string (pid, n, _) = Printf.sprintf "%s_%s" pid n in 
    let to_string_var (pid, n, v) =
      Printf.sprintf "%s_%s=%s" pid n (Data.val_to_string string_of_float v)
    in
    let vars = List.rev vars in
    Printf.sprintf "[%i%s] %s\n" e.step
      (if e.step <> e.nb then (":" ^ (string_of_int e.nb)) else "")
      (String.concat " " (List.map to_string_var vars))
      
let print_sasa_event e =
  if e.kind <> Ltop then print_event e else
    (
      print_string (str_of_sasa_event e);
      flush stdout
    )

let _ =
  del_hook "print_event";
  add_hook "print_event" print_sasa_event
       
(**********************************************************************)
(* handy short-cuts *)
let roundnb = ref 0
let roundtbl = Hashtbl.create 1

let _ = time_travel true;; 
let e = ref (RdbgStdLib.run());;
let s () = e:=step  !e  ; emacs_udate !e; store !e.nb;;
let si i = e:=stepi !e i; emacs_udate !e; store !e.nb;;
let n () = e:=next  !e  ; emacs_udate !e; store !e.nb;;
let ni i = e:=nexti !e i; emacs_udate !e; store !e.nb;;
let g i  = e:=goto  !e i; emacs_udate !e; store !e.nb;;
let b () =
  e:=back !e;
  roundnb := Hashtbl.find roundtbl !e.nb;
  emacs_udate !e; store !e.nb;;
let bi i = e:=backi !e i; roundnb := Hashtbl.find roundtbl !e.nb;
  emacs_udate !e; store !e.nb;;
let r () = !e.RdbgEvent.reset(); e:=RdbgStdLib.run();
  roundnb := 1;
  Hashtbl.clear roundtbl;
  emacs_udate !e; store !e.nb;;
let c () = e:= continue !e;emacs_udate !e; store !e.nb;;
let cb () = e:= rev !e;roundnb := Hashtbl.find roundtbl !e.nb;
  emacs_udate !e; store !e.nb;;
let fc predicate =
   e := next_np !e; while not (predicate ()) do e := next_np !e; done;
   get_hook "print_event" !e;
   emacs_udate !e;
   store !e.nb;;
let bc predicate =
   e := back !e; while not (predicate ()) do e := back !e; done;
   get_hook "print_event" !e;
   roundnb := Hashtbl.find roundtbl !e.nb;
   emacs_udate !e;
   store !e.nb;;

let sinfo () = match !e.sinfo with Some si -> si() | None  -> failwith "no source info";;
let undo () =
  match !redos with
  | _::i::t ->
    redos := i::t; e:=goto !e i;emacs_udate !e;
    roundnb := Hashtbl.find roundtbl !e.nb;
  | _ -> e:=goto !e 1; emacs_udate !e
;;
let u = undo ;;
let vv str = v str !e;;

let _ =
  add_doc_entry "s" "unit -> unit" "Moves forward of one step"
     "move" "my-rdbg-tuning.ml";
  add_doc_entry "si" "int -> unit" "Moves forward of n steps"
     "move" "my-rdbg-tuning.ml";
  add_doc_entry "n" "unit -> unit" "Moves forward of one event" 
     "move" "my-rdbg-tuning.ml";
  add_doc_entry "ni" "int -> unit" "Moves forward of i events" 
     "move""my-rdbg-tuning.ml";
  add_doc_entry "g" "int -> unit" "Goes to event number i" 
     "move" "my-rdbg-tuning.ml";
  add_doc_entry "b" "unit -> unit" "Moves backward of one event" 
     "move" "my-rdbg-tuning.ml";
  add_doc_entry "bi" "int -> unit" "Moves backward of i events" 
     "move" "my-rdbg-tuning.ml";
  add_doc_entry "bc" "(unit -> bool) -> unit" 
      "Moves backward until a predicate (of type unit -> bool) becomes true
      " "move" "my-rdbg-tuning.ml";
  add_doc_entry "fc" "(unit -> bool) -> unit" 
      "Moves forward until a predicate (of type unit -> bool) becomes true" 
      "move" "my-rdbg-tuning.ml";
  add_doc_entry "u" "unit -> unit" "Undo the last move"  
      "move" "my-rdbg-tuning.ml";
  add_doc_entry "vv" "string -> Data.v" 
   "Returns the value of a variable attached to the current event (if available)" 
    "data" "my-rdbg-tuning.ml";
  add_doc_entry "r" "unit -> unit" "Restarts to the first event" 
    "main" "my-rdbg-tuning.ml";
  add_doc_entry "l" "unit -> unit" "Prints this list of L0 commands description" 
    "main" "my-rdbg-tuning.ml";
  add_doc_entry "where" "unit -> unit"
    "Prints the call stack with source information"  
    "misc" "my-rdbg-tuning.ml"; 
  add_doc_entry "sinfo" "unit -> unit" 
     "Returns source information attached to the current event (if available)" 
     "data" "my-rdbg-tuning.ml";
  add_doc_entry "c" "unit -> unit" "Continues forward until the next breakpoint" 
     "move" "my-rdbg-tuning.ml";
  add_doc_entry "cb" "unit -> unit" "Continues backward until the previous breakpoint"  
    "move" "my-rdbg-tuning.ml";
  add_doc_entry "blist" "unit -> unit" "Lists of the current breakpoints" 
     "move" "my-rdbg-tuning.ml";

  add_doc_entry "nm" "string -> unit"
     "Moves forward until an event which name matches a <string>"  
    "move" "my-rdbg-tuning.ml";
  add_doc_entry "pm" "string -> unit"
    "Moves backward until a function which name matches a <string>"  
    "move" "my-rdbg-tuning.ml";
  add_doc_entry "exit" "unit -> unit" "Goes to the exit of the current event" 
    "main" "my-rdbg-tuning.ml"


(**********************************************************************)
open Callgraph;;
let cgf () = gen_call_graph_full !e;;
let cg () = (gen_call_graph !e);;
let dcg ()= ignore(display_call_graph ());;  
let bt () = print_src !e;;
let finish () = loopforever !e;;
let where () = print_call_stack !e;;

let _ = 
  add_doc_entry "cgf" "unit -> unit" 
    "Generates the full call graph recursively (i.e., nodes are clickable) "
    "graph" "my-rdbg-tuning.ml" ;
  add_doc_entry "cg" "unit -> unit" 
    "Generates the call graph from the current event"  
    "graph" "my-rdbg-tuning.ml";
  add_doc_entry "dcg" "unit -> unit" "Displays the generated call graph" 
   "graph" "my-rdbg-tuning.ml"
;;

(**********************************************************************)
(* breakpoints *)

let blist () = !breakpoints;;

(**********************************************************************)
(* Go to the next event which name contains a string (useful for
   navigating inside lustre or lutin programs) *)
let nm str = e:= next_match  !e str ;;
let pm str = e:=previous_match !e str ;;

let next_cond e c =
  let e = next_cond e c in
  store e.nb;
  e

(* go to the exit of the current event *)
let exit () = 
   if !e.kind <> Exit then e:=next_cond !e 
     (fun ne-> ne.kind = Exit && ne.name = !e.name && ne.depth = !e.depth) ;;
(**********************************************************************)
(* Online Level 0 doc *)

let l () = print_string ("
Here is the list of rdbg Level 0 commands (i.e., defined in my-rdbg-tuning.ml) :
- forward moves:
  n:    "^(RdbgMain.doc_msg "n")^" 
  ni i: "^(RdbgMain.doc_msg "ni")^"
  s:    "^(RdbgMain.doc_msg "s")^"
  si i: "^(RdbgMain.doc_msg "si")^"
  nm <string>: "^(RdbgMain.doc_msg "nm")^"
  fc pred:"^(RdbgMain.doc_msg "fc")^"
  exit:  "^(RdbgMain.doc_msg "exit")^"

- backward moves:
  b:    "^(RdbgMain.doc_msg "b")^"
  bi i: "^(RdbgMain.doc_msg "bi")^"
  g i:  "^(RdbgMain.doc_msg "g")^"
  pm <string>:   "^(RdbgMain.doc_msg "pm")^"
 
- Call graphs (requires GraphViz dot)
  cg:  "^(RdbgMain.doc_msg "cg")^"
  cgf: "^(RdbgMain.doc_msg "cgf")^"
  dcg: "^(RdbgMain.doc_msg "dcg")^"
 
- Breakpoints:
  break <brpt>: "^(RdbgMain.doc_msg "break")^"
  c:  "^(RdbgMain.doc_msg "c")^"
  cb: "^(RdbgMain.doc_msg "cb")^"
  blist:  "^(RdbgMain.doc_msg "blist")^"
  delete: "^(RdbgMain.doc_msg "delete")^"

- misc:
  where: "^(RdbgMain.doc_msg "where")^"
  sinfo: "^(RdbgMain.doc_msg "sinfo")^"
  vv:  "^(RdbgMain.doc_msg "vv")^"

- main:
  l: "^(RdbgMain.doc_msg "l")^"
  i: "^(RdbgMain.doc_msg "i")^"
  r: "^(RdbgMain.doc_msg "r")^"
  u: "^(RdbgMain.doc_msg "u")^"
  q: "^(RdbgMain.doc_msg "q")^"
");;

let man () = man();
  output_string stdout "   + l (* List of Level 0 (sasa specific) commands *)
";
  flush stdout
    ;;

(* The  code   above  was  (probably)  generated  by   rdbg  when  no
   my-rdbg-tuning.ml file was present in this directory *)
(**********************************************************************)
(**********************************************************************)

(* The code below is specific to sasa and to the sasa test directory *)

#mod_use "../../lib/algo/algo.ml";; 
#mod_use "../../lib/sasacore/register.ml";;
#mod_use "../../lib/sasacore/topology.ml";;
#use "../rdbg-utils/dot.ml";;

let p = Topology.read dotfile;;

(* shortcuts for dot viewer *)
let d_par () = dot true p dotfile !e;;
let dot_par () = dot true p dotfile !e;;
let ne_par () = neato true p dotfile !e;;
let tw_par () = twopi true p dotfile !e;;
let ci_par () = circo true p dotfile !e;;
let fd_par () = fdp true p dotfile !e;;
let sf_par () = sfdp true p dotfile !e;;
let pa_par () = patchwork true p dotfile !e;;
let os_par () = osage true p dotfile !e;;

let dot () = dot false p dotfile !e;;
let ne () = neato false p dotfile !e;;
let tw () = twopi false p dotfile !e;;
let ci () = circo false p dotfile !e;;
let fd () = fdp false p dotfile !e;;
let sf () = sfdp false p dotfile !e;;
let pa () = patchwork false p dotfile !e;;
let os () = osage false p dotfile !e;;


(* To be able to choose the different default dot viewer on a per  directory basis.
   To change the graph printer: 
   dot_view := ci;;
*)
let dot_view : (unit -> unit) ref =
  ref ( (* choosing a reasonable default viewer *)
      if String.length dotfile > 4 && String.sub dotfile 0 4 = "ring" then ci else
      if Algo.is_directed () then dot else ne)

let d = !dot_view 

(* Move forward until convergence, i.e., when no data changes *)
let rec go () =
  let data = !e.data in
  s(); if data = !e.data then !dot_view () else go () ;;

let sd () = s();!dot_view();;
let nd () = n();!dot_view();;
let bd () = b();!dot_view();;

(**********************************************************************)
(** Computing rounds *)

(* a process can be removed from the mask if one action of p is triggered
  or if no action of p is enabled *)
let get_removable pl =
  let pl = List.filter
      (fun p ->
         (List.exists (fun (_,_,acti) -> acti) p.actions) ||
         (List.for_all (fun (_,enab,_) -> (not enab)) p.actions)
      )
      pl
  in
  List.map (fun p -> p.name) pl

let verbose = ref false

let last_round = ref 0
let mask = ref [] (* nodes we look the activation of *)


(* called at each event via the time-travel hook *)
let (round : RdbgEvent.t -> bool) =
  fun e ->
  e.kind = Ltop &&
    let (pl : process list) = get_processes e in
    let rm_me = get_removable pl in
    if !verbose then (
      Printf.printf "Mask : %s\n" (String.concat "," !mask); 
      Printf.printf "To remove from mask: %s\n" (String.concat "," rm_me); 
      flush stdout;
    );
    mask := List.filter (fun pid -> not (List.mem pid rm_me)) !mask;
    let res = !mask = [] ||
              (* when round is called twice, it should have the same
                 result *) 
              !last_round = e.nb 
    in
    if !mask = [] then (
      last_round := e.nb;
      mask := (
        let p_with_enable_action =
          List.filter
            (fun p -> List.exists
                (fun (_,enab,acti) -> enab && not(acti)) p.actions)
            pl
        in
        let pidl = List.map (fun p -> p.name) p_with_enable_action in
        let pidl = List.rev pidl in
        if !verbose then (
          Printf.printf "Next mask : %s\n" (String.concat "," pidl);
          flush stdout
        );
        pidl
      );
      try roundnb := Hashtbl.find roundtbl e.nb
      with _ -> (
          incr roundnb;
          Hashtbl.add roundtbl e.nb !roundnb;
        )
    );
    res
 
(* go to next and previous rounds  *)
let next_round e = next_cond e round;;
let nr () = e:=next_round !e; !dot_view ();;
let pr () =
  e:=goto_last_ckpt !e.nb;
  roundnb := Hashtbl.find roundtbl !e.nb;
  !dot_view ();;

(* shortcuts to the default and sasa event printers *)
let pe () = print_event !e;;
let spe () = print_sasa_event !e;;

(**********************************************************************)
(* Goto to the next Lustre oracle violation *)
let goto_next_false_oracle e =
  next_cond e (fun e -> e.kind = Exit && e.lang = "lustre" &&
                        List.mem ("ok", Bool) e.outputs  &&
                        not (vb "ok" e))

let viol () = e:=goto_next_false_oracle !e; !dot_view ();;

(**********************************************************************)
(* Move forward until silence *)

let is_silent e =
  let enab, _act, _vars = split_data e.data in
  List.for_all (fun (_,_,v) -> v = Data.B false) enab

let goto_silence e = next_cond e is_silent 
let silence () = e:=goto_silence !e; !dot_view ();;

(**********************************************************************)
(* Perform the checkpointing at rounds! *)
let _ = check_ref := round;;

(**********************************************************************)
let _ = 
  add_doc_entry "d" "unit -> unit" "display the current network with dot" "sasa" "my-rdbg-tuning.ml";
  add_doc_entry "ne" "unit -> unit" "display the current network with neato" "sasa" "my-rdbg-tuning.ml";
  add_doc_entry "tw" "unit -> unit" "display the current network with twopi" "sasa" "my-rdbg-tuning.ml";
  add_doc_entry "ci" "unit -> unit" "display the current network with circo" "sasa" "my-rdbg-tuning.ml";
  add_doc_entry "fd" "unit -> unit" "display the current network with fdp" "sasa" "my-rdbg-tuning.ml";
  add_doc_entry "sf" "unit -> unit" "display the current network with sfdp" "sasa" "my-rdbg-tuning.ml";
  add_doc_entry "pa" "unit -> unit" "display the current network with patchwork" "sasa" "my-rdbg-tuning.ml";
  add_doc_entry "os" "unit -> unit" "display the current network with osage" "sasa" "my-rdbg-tuning.ml";
  
  add_doc_entry "sd" "unit -> unit"
    "go to the next step and display the network with one of the GraphViz tools" "sasa" "my-rdbg-tuning.ml";
  add_doc_entry "nd" "unit -> unit" "go to the next event and display the network" "sasa" "my-rdbg-tuning.ml";
  add_doc_entry "bd" "unit -> unit" "go to the previous event and display the network" "sasa" "my-rdbg-tuning.ml";
  add_doc_entry "nr" "unit -> unit" "go to the next round and display the network" "sasa" "my-rdbg-tuning.ml";
  add_doc_entry "pr" "unit -> unit" "go to the previous round and display the network" "sasa" "my-rdbg-tuning.ml"
  ;;

let l () =
  l();
  print_string ("
- sasa commands
 + Display network with GraphViz tools
   d: "^(RdbgMain.doc_msg "d")^"
   ne: "^(RdbgMain.doc_msg "ne")^"
   tw: "^(RdbgMain.doc_msg "tw")^" 
   ci: "^(RdbgMain.doc_msg "ci")^"
   fd: "^(RdbgMain.doc_msg "fd")^"
   sf: "^(RdbgMain.doc_msg "sf")^"
   pa: "^(RdbgMain.doc_msg "pa")^"
   os: "^(RdbgMain.doc_msg "os")^"
   
   nb: for algorithms that have a field named 'par', you can try 
   of the following (which only draw the parent arcs) 
 
   d_par: "^(RdbgMain.doc_msg "d")^" (parent arcs only)
   ne_par: "^(RdbgMain.doc_msg "ne")^" (parent arcs only)
   tw_par: "^(RdbgMain.doc_msg "tw")^" (parent arcs only)
   ci_par: "^(RdbgMain.doc_msg "ci")^" (parent arcs only)
   fd_par: "^(RdbgMain.doc_msg "fd")^" (parent arcs only)
   sf_par: "^(RdbgMain.doc_msg "sf")^" (parent arcs only)
   pa_par: "^(RdbgMain.doc_msg "pa")^" (parent arcs only)
   os_par: "^(RdbgMain.doc_msg "os")^" (parent arcs only)

 + Moving commands [*]
   sd: "^(RdbgMain.doc_msg "sd")^"
   nd: "^(RdbgMain.doc_msg "nd")^"
   bd: "^(RdbgMain.doc_msg "bd")^"
   nr: "^(RdbgMain.doc_msg "nr")^"
   pr: "^(RdbgMain.doc_msg "pr")^"
 [*] in order to change the current graph drawing engine, you can use
the dot_view command as follows:
  dot_view := ci
");;

(**********************************************************************)
(* is the current configuration stable? *)
let count_true l =
  List.fold_left (fun cpt (_,_,v) -> if v=B true then cpt+1 else cpt) 0 l

let stable_event e =
  let enab, _, _ = split_data e.data in
  count_true enab = 0
let stable () = stable_event !e 

let _ = add_doc_entry
    "stable" "unit -> bool" "is the current configuration stable?" "sasa"
    "my-rdbg-tuning.ml"
 

(**********************************************************************)
(* ok, let's start the debugging session! *)

let pdf_viewer =
  if Sys.command "which zathura" = 0 then "zathura" else
  if Sys.command "which xpdf" = 0 then "xpdf" else
  if Sys.command "which acroread" = 0 then "acroread" else
  if Sys.command "which evince" = 0 then "evince" else (
    Printf.printf "Warning: no pdf viewer is found to visualize %s\n%!" dotfile;
    "ls"
  )
  
let graph_view () = 
  !dot_view ();
  let cmd = Printf.sprintf "%s sasa-%s.pdf&" pdf_viewer dotfile in
  Printf.printf "%s\n!" cmd;
  ignore(Sys.command cmd); 
  ignore(round !e)

 ;;
let _ = graph_view ()
