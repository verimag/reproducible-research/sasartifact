(* Time-stamp: <modified the 21/04/2021 (at 13:40) by Erwan Jahier> *)

open Algo

(* let m=10 (* max 2 (1+2*diameter ()) *) *)
let diameter =
   try int_of_string (Algo.get_graph_attribute "diameter")
   with _ -> diameter()
    
let m = max 2 (1+2*diameter) 

let (init_state: int -> string -> 'v) =
  fun _ _ ->
    (*     Printf.eprintf "unison.ml: tossing!\n";flush stderr;  *)
    Random.int m

let list_min l =
  match l with
    [] -> assert false
  | x::l -> List.fold_left min x l

let new_clock_value nl clock =
  let cl = List.map (fun n -> state n) nl in
  let min_clock = List.fold_left min clock cl in
  (min_clock + 1) mod m 

let (enable_f: 'v -> 'v neighbor list -> action list) =
  fun clock nl ->
    if (new_clock_value nl clock) <> clock then ["g"] else []
  
let (step_f : 'v -> 'v neighbor list -> action -> 'v) =
  fun clock nl a ->
    let v = new_clock_value nl clock in
    match a with
    | _ -> v



