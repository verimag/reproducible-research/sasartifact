(* Time-stamp: <modified the 22/04/2020 (at 10:39) by Erwan Jahier> *)

(* A variant of test/alea-coloring:

   Algo 3.3.1 (page 16) of Self-stabilizing Vertex Coloring of Arbitrary Graphs
   by Maria Gradinariu and Sebastien Tixeuil

   http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.38.8441&rep=rep1&type=pdf
*)

open Algo
open State

let b=max_degree ()
    
let (init_state: int -> string -> 'v) =
  fun _k _id -> { id = "anonymous" ; r = 0 }

let free_color cl = (* returns the max color that is not in cl, which is sorted *)
  let rec f c cl =
    match cl with
    | [] -> c
    | x::t ->
      if c > x then c (* x is the max, so c is free *)
      else if c = x then f (c-1) t (* c is not free: try a lower color *)
      else assert false (* should not occur as cl is sorted *)
  in
  f b cl

(* Returns the list of used colors, in ascending order *)
let (used_colors : 'v neighbor list -> int list) = fun nl -> 
  let color s = (state s).r in
  let cl = List.map color nl in 
  List.sort_uniq (fun x y -> compare y x) cl

let agree i cl = i = free_color cl

let (enable_f: 'v -> 'v neighbor list -> action list) =
  fun c nl ->
  if not (agree c.r (used_colors nl)) then ["C1"] else []
  
let (step_f : 'v -> 'v neighbor list -> action -> 'v) =
  fun e nl ->
  function
  | "C1" -> if (Random.bool ()) then e else { e with r = free_color (used_colors nl) }
  | _ -> e
           



