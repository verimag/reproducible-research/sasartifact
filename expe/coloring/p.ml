(* Time-stamp: <modified the 05/03/2020 (at 21:09) by Erwan Jahier> *)

(* This is algo 3.1 in the book *)

open Algo

let k=max_degree ()

let (init_state: int -> string -> 'v) = fun _i _ -> Random.int k 

let verbose = false 
let (state_to_string: ('v -> string)) = string_of_int
let (copy_state : ('v -> 'v)) = fun x -> x

let (neigbhors_values : 'v neighbor list -> 'v list) =
  fun nl ->
    List.map (fun n -> state n) nl 

let (clash : 'v  -> 'v neighbor list -> bool) = fun v nl -> 
  let vnl = neigbhors_values nl in
  let res = List.mem v vnl in
  res

let (free : 'v neighbor list -> 'v list) = fun nl ->
  let clash_list = List.sort_uniq compare (neigbhors_values nl) in
  let rec aux free clash i =
    if i > k then free else
      (match clash with
       | x::tail ->
         if x = i then aux free tail (i+1) else aux (i::free) clash (i+1)
       | [] -> aux (i::free) clash (i+1)
      )
  in
  let res = aux [] clash_list 0 in
  List.rev res

let (enable_f: 'v -> 'v neighbor list -> action list) =
  fun e nl ->
  if (clash e nl) then ["conflict"] else []

let (step_f : 'v -> 'v neighbor list -> action -> 'v) =
  fun e nl a ->
    let f = free nl in
    if f = [] then e else
      match a with
      | _ -> List.hd f 


