(* Time-stamp: <modified the 05/03/2020 (at 21:28) by Erwan Jahier> *)

(* cf Collin-Dolex-94 *)

open Algo
open State
    
let (init_state: int -> string -> 'v) =
  fun i _ ->
    {
      path = [-1];
      par  = -10
    }

let (enable_f: 'v -> 'v neighbor list -> action list) =
  fun v _nl ->
    if v.path = [-1] then [] else ["update_path"]

   
let (step_f : 'v -> 'v neighbor list -> action -> 'v) =
  fun v nl -> 
    function
    | "update_path" ->  { v with path = [-1] }
    | _ -> assert false
