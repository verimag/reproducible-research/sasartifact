(* Time-stamp: <modified the 05/03/2020 (at 20:33) by Erwan Jahier> *)

open Algo

let n = Algo.card()
let k = n * n + 1

let (init_state: int state_init_fun) = fun _n _ -> (Random.int k)

let modulo x n = if x < 0 then n+x mod n else x mod n 

let behind pc qc = (modulo (qc-pc) k) <= n

let (enable_f: int enable_fun) =
  fun c nl ->
    if List.for_all (fun q -> behind c (state q)) nl then ["I(p)"] else
    if List.exists  (fun q -> not (behind c (state q)) &&
         		               not (behind (state q) c)) nl
       && c <> 0 then ["R(p)"] else []

let (step_f: int step_fun) =
  fun c nl a ->
    match a with
    | "I(p)" -> modulo (c + 1) k
    | "R(p)" -> 0  
    | _      -> assert false
    
