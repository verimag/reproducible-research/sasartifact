# Time-stamp: <modified the 18/04/2021 (at 13:28) by Erwan Jahier>
#
# This Makefile is meant to be includes by directories below

.PRECIOUS: .ml .rif .cmxs .dot
%.ml: %.dot
	rm -f config.ml
	sasa -reg $<
	touch config.ml # so that it compiles under sasa 4.0.3

%.cmxs: %.ml
	ocamlfind ocamlopt -package algo -shared state.ml $(shell sasa -algo $*.dot) config.ml $< -o $@


%_oracle.lus: %.dot %.cmxs
	sasa -glos $< || echo "==> ok, I'll use the existing $@ file"



##################################################################################

%.log: %.dot %.cmxs 
	/usr/bin/time -v -o $@ sasa --no-data-file $(DAEMON) -l $(N) $*.dot > $*.rif 2> log ; cat log >> $@


##################################################################################
genclean:
	rm -f *.cmxs sasa *.cm* *.o *.pdf *.rif *.gp *.log* *.dro *.seed *.c *.h sasa-*.dot
	rm -f rdbg-session*.ml luretteSession*.ml *.lut a.out *.cov
	rm -f *.exec *.sh

