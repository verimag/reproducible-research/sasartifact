(* Time-stamp: <modified the 05/03/2020 (at 21:43) by Erwan Jahier> *)

(* cf Collin-Dolex-94 *)

open Algo
open State
    
let (init_state: int -> string -> State.t) =
  fun i _ ->
    {
      path = Array.make delta (-1);
      par  = -10
    }

let (enable_f: t -> t neighbor list -> action list) =
  fun v _nl ->
    if v.path = (Array.make delta (-1)) then [] else ["update_path"]

   
let (step_f : t -> t neighbor list -> action -> t) =
  fun v nl -> 
    function
    | "update_path" ->  { v with path = Array.make delta (-1) }
    | _ -> assert false

