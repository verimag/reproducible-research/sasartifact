

type t = { root: bool ; v : int } (* semi-anonymous network: we know who is the root! *)
let to_string s = Printf.sprintf "c=%i" s.v
let (of_string: (string -> t) option) =
  Some (fun s ->
    Scanf.sscanf s "{root=%d;v=%d}" (fun i1 i2 -> { root = i1<>0; v = i2 } ))
let copy x = x
let actions = ["T"]
