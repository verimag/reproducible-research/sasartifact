(* Time-stamp: <modified the 01/09/2020 (at 17:18) by Erwan Jahier> *)

open Algo

let k = card()
open State
let (init_state: int -> string -> 's) =
  fun _ _ ->
    (*     let k = (card() - 1) in *)
    (*     let _ = assert (k > 0) in *)
    { root = false ; v = Random.int k }

let (enable_f: 's -> 's neighbor list -> action list) =
  fun e nl ->
    let pred = match nl with [n] -> n | _ -> assert false in
    if e.v <> (state pred).v then ["T"] else []
  
let (step_f : 's -> 's neighbor list -> action -> 's) =
  fun e nl a ->
    let pred = match nl with [n] -> n | _ -> assert false in
    match a with 
    | _ ->  { e with v = (state pred).v }



