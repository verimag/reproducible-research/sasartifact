(* Time-stamp: <modified the 05/03/2020 (at 21:41) by Erwan Jahier> *)

(* This is algo 5.3 in the book *)

open Algo
open State
    
let (init_state: int -> string -> State.t) =
  fun i _ ->
    {
      d = Random.int d;
      par = -1;
    }   
 
let (enable_f: State.t -> State.t neighbor list -> action list) =
  fun st nl ->
    if st.d <> 0 then ["CD"] else []
  
let (step_f : State.t -> State.t neighbor list -> action -> State.t) =
  fun st _nl ->
    function  | _ -> { st with d = 0 }

