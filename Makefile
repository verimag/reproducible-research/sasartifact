

all:
	cd expe ; make && make save_result

bigexpe:
	cd expe ; make -j 5 && make bigexpe -j 5 && make save_result

git:
	git commit -a -F log

amend:
	git commit -a -F log --amend

ci:
	gitlab-runner exec docker  --docker-dns 152.77.1.22 expe-sasa.4.0.3


cih:
	gitlab-runner exec docker  --docker-dns 8.8.8.8 expe-sasa.4.0.3
	gitlab-runner exec docker  --docker-dns 8.8.8.8 expe-current
	gitlab-runner exec docker  --docker-dns 8.8.8.8 expe-dockver

cih1:
	gitlab-runner exec docker  --docker-dns 8.8.8.8 expe-current
