#use "genExpeMakefiles.ml";;
precision := 0.1;;
    (* 0.1 means that we simulate until the Confidence Interval
       size of  the 3 complexity  numbers under estimation  is smaller
       than 10% of their current estimation.
    *)
let algos = ["../../test/alea-coloring-alt";
             "../../test/alea-coloring-unif";
             "../../test/alea-coloring"]
let daemons = ["-sd"; "-lcd"; "-dd"] 
let cliques = List.init 10 (fun n -> Clique (30*(n+1))) (* Cliques of size 30, 60, ..., 300 *)
let er = List.init 10 (fun n -> ER (30*(n+1), 0.4))     (* ER of size 30, 60, ..., 300 *)
let rings = List.init 10 (fun n -> Ring (500*(n+1)))    (* Rings of size 500, 1000, ..., 5000 *)
let networks = (cliques@rings@er)

let gen_make_rules () = gen_makefile "Makefile.expe-rules" daemons algos networks;;

#use "parseLog.ml";;
let gen_pdf () =
  let gl = ["clique"; "ring"; "er"] in 
  List.iter (fun n -> sh ("rm -f "^n^".data")) gl; 
  parse_log ["Uniform When Activated","alea-coloring-unif"] gl daemons;
  parse_log ["Smallest When Activated","alea-coloring"] gl daemons;
  parse_log ["Always the Biggest","alea-coloring-alt"] gl daemons;    
  List.iter (fun n -> sh ("./gen_pdf_paper.r "^n^".data coloring")) gl;
  ()
;;
